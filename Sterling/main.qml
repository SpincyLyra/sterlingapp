import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.5

Window
{
    visible: true;  width: 640; height: 480; title: qsTr("Sterling")
    readonly property string delegateTextColor: "#4c4c4c"

Rectangle
{
    height: parent.height; width: parent.width; color: "black"; opacity: 1.0
    id: parentRectId
    ListView
    {
        id: mlistSelectBtnId; anchors.fill: parent; spacing: 70;
        header:headerId; height: parentRectId.height; width: parentRectId.width

        model: ["B2B", "AMP-HR", "REMOTE CONN"]

        highlight: Rectangle
        {
            width: parent.width; color:"blue"; radius:14; border.color:"yellowgreen"
            z:2; opacity: 0.1; anchors.horizontalCenter: parent.horizontalCenter
        }
        delegate:delegateComponent
    }
}

Component
{
    id: headerId
    Rectangle
    {
        id: headerRectID
        width: parent.width; height: 70; color:"gray"; opacity: 0.0
        Text
        {
            anchors.centerIn: parent
            text: " "
            font.pointSize: 20
        }
    }
}

Component
{
    id: delegateComponent
    Rectangle
    {
        id: rectangleId; width: parent.width-height; height: 60;opacity: .8
        radius:20; color: delegateTextColor; border.color: "gainsboro"
        anchors.horizontalCenter: parent.horizontalCenter

        Text
        {
            id: textId; anchors.centerIn: parent; text: modelData
            color: "lime"; font{pointSize: 20; family:"consolas" }
        }
        MouseArea
        {
            anchors.fill:parent
            onClicked:
            {
                //rectangleId.color = "silver"
                mlistSelectBtnId.currentIndex = index
                console.log(mlistSelectBtnId.currentIndex)
            }
        }
    }
}
}
